import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MultiplicationWithoutMultiplicationTest {
    private int firstValue, secondValue;
    //
    @Test
    void positiveOperation() {
        int firstValue = 11;
        int secondValue = 11;
        assertEquals(firstValue * secondValue, Multiplication.multiplications(firstValue, secondValue));
    }
    @Test
    void negativeOperation() {
        firstValue = -11;
        secondValue = -11;
        assertEquals(firstValue * secondValue, Multiplication.multiplications(firstValue, secondValue));
    }

    @Test
    void positiveAndNegative() {
        firstValue = -11;
        secondValue = 11;
        assertEquals(firstValue * secondValue, Multiplication.multiplications(firstValue, secondValue));
    }
    @Test
    void negativeAndPositive() {
        firstValue = 11;
        secondValue = -11;
        assertEquals(firstValue * secondValue, Multiplication.multiplications(firstValue, secondValue));
    }
    @Test
     void zeroOperation() {
        firstValue = 0;
        secondValue = 0;
        assertEquals(firstValue * secondValue, Multiplication.multiplications(firstValue, secondValue));
    }
    @Test
     void positiveAndZero () {
        firstValue = 11;
        secondValue = 0;
        assertEquals(firstValue * secondValue, Multiplication.multiplications(firstValue, secondValue));

    }
}
